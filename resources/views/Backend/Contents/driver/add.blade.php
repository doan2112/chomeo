@extends('Backend.Layouts.default')
@section('content')
    <div id="content-container">
        <div id="page-head">
            <div id="page-title">
                <h1 class="page-header text-overflow"> Lái xe</h1>
            </div>
            <ol class="breadcrumb">
                <li><a href="#"><i class="demo-pli-home"></i></a></li>
                <li><a href="#">{{ isset($driver) ? 'Cập nhật' : 'Thêm mới' }}</a></li>
            </ol>
        </div>
        <div id="page-content">
            <div class="panel-body">
                <div class="panel">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-heading">
                                <h3 class="panel-title text-main text-bold mar-no">
                                    <i class="ti-pencil"></i> {{ isset($driver) ? 'Cập nhật' : 'Thêm mới' }}
                                </h3>
                            </div>
                        </div>
                        <div class="panel-body col-sm-offset-2">
                            <!-- data-parsley-validate -->
                            @if (isset($driver))
                                <form action="{{ route('drivers.update', @$driver->id) }}" method="POST" enctype="multipart/form-data" >
                                    @method ('PUT')
                            @else
                                <form action="{{ route('drivers.store') }}" method="POST" enctype="multipart/form-data" >
                                    @method ('POST')
                            @endif
                                @csrf
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Mã tài xế
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" @if(@$driver) readonly @endif  name="code" class="form-control"
                                                       value="{{ @$driver->code ? $driver->code : @old('code') }}">
                                                @if ($errors->has('code'))
                                                    <p class="text-left text-danger">{{ $errors->first('code') }}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Tên tài xế
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" name="name" class="form-control"
                                                       value="{{ @$driver->user->name ? $driver->user->name : @old('name') }}">
                                                @if ($errors->has('name'))
                                                    <p class="text-left text-danger">{{ $errors->first('name') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Tuổi
                                                </label>
                                                <input type="text" name="old" class="form-control"
                                                       value="{{ @$driver->old ? $driver->old : @old('old') }}">
                                                @if ($errors->has('old'))
                                                    <p class="text-left text-danger">{{ $errors->first('old') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Số điện thoại
                                                </label>
                                                <input type="text" name="phone" class="form-control"
                                                       value="{{ @$driver->user->phone ? $driver->user->phone : @old('phone') }}">
                                                @if ($errors->has('phone'))
                                                    <p class="text-left text-danger">{{ $errors->first('phone') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Email
                                                </label>
                                                <input type="text" @if(@$driver) readonly @endif name="email" class="form-control"
                                                       value="{{ @$driver->user->email ? $driver->user->email : @old('email') }}">
                                                @if ($errors->has('email'))
                                                    <p class="text-left text-danger">{{ $errors->first('email') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Địa chỉ
                                                </label>
                                                <input type="text" name="address" class="form-control"
                                                       value="{{ @$driver->address ? $driver->address : @old('address') }}">
                                                @if ($errors->has('address'))
                                                    <p class="text-left text-danger">{{ $errors->first('address') }}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Loại xe</label>
                                                <select class="selectpicker" data-live-search="true" data-width="100%" name="category_car">
                                                    @foreach ($categories_car as $key => $cate)
                                                        <option @if ($key === $driver->category_car) selected @endif  value="{{ $key }}">-- {{ $cate }} --</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('category_car'))
                                                    <p class="text-left text-danger">{{ $errors->first('category_car') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Số chứng minh thư
                                                </label>
                                                <input type="text" name="card_id" class="form-control"
                                                       value="{{ @$driver->card_id ? $driver->card_id : @old('card_id') }}">
                                                @if ($errors->has('card_id'))
                                                    <p class="text-left text-danger">{{ $errors->first('card_id') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Số bằng lái xe
                                                </label>
                                                <input type="text" name="licence_id" class="form-control"
                                                       value="{{ @$driver->licence_id ? $driver->licence_id : @old('licence_id') }}">
                                                @if ($errors->has('licence_id'))
                                                    <p class="text-left text-danger">{{ $errors->first('licence_id') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Biển số xe
                                                </label>
                                                <input type="text" name="license_plates" class="form-control"
                                                       value="{{ @$driver->license_plates ? $driver->license_plates : @old('license_plates') }}">
                                                @if ($errors->has('license_plates'))
                                                    <p class="text-left text-danger">{{ $errors->first('license_plates') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Đội xe
                                                </label>
                                                <select class="selectpicker" data-live-search="true" data-width="100%" name="group_id">
                                                    @foreach ($groups as $key => $group)
                                                        <option @if ($group->id === $driver->category_car) selected @endif  value="{{ $group->id }}">-- {{ $group->name }} --</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('group_id'))
                                                    <p class="text-left text-danger">{{ $errors->first('group_id') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold">
                                                    Ảnh <span class="text-danger"> (*)</span>
                                                </label>
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <a data-input="thumbnail" data-preview="holder" class="btn btn-primary my-lfm" type="'image'">
                                                            <i class="fa fa-picture-o"></i> Chọn ảnh
                                                        </a>
                                                    </span>
                                                    <input id="thumbnail" class="form-control" type="text" name="url_image" value="{{ @$driver->image ? $driver->image: @old('url_image') }}">
                                                </div>
                                                <img id="holder"
                                                     @if (@$product->image || @old('url_image'))
                                                     src="{{ url('') }}/{{ @$driver->image ? $driver->image : @old('url_image') }}"
                                                     @endif style="margin-top:15px;max-height:100px;">
                                                     @if ($errors->has('url_image'))
                                                        <p class="text-left text-danger">{{ $errors->first('url_image') }}</p>
                                                    @endif
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block btn-form-submit"><i class="ti-save"></i></button>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('myJs')
    <script>
        $('.selected-2').select2();
    </script>
@endsection