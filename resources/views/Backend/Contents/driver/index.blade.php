@extends('Backend.Layouts.default')
@section('content')
    <div id="content-container" ng-controller="driverCtrl">
        <div id="page-head">
            <div id="page-title">
                <h1 class="page-header text-overflow">Lái xe</h1>
            </div>
            <ol class="breadcrumb">
                <li><a href="#"><i class="demo-pli-home"></i></a></li>
                <li><a href="#">Danh sách</a></li>
            </ol>
        </div>
        <div id="page-content">
            <div class="panel-body">
                <div class="panel">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div class="pad-btm form-inline">
                            <div class="row">
                                <div class="col-sm-6 table-toolbar-left">
                                    <a href="{{ route('drivers.create') }}" id="demo-btn-addrow" class="btn btn-purple">
                                        <i class="demo-pli-add"></i>Thêm mới
                                    </a>
                                </div>
                                <div class="col-sm-6 table-toolbar-right">
                                    <div class="form-group col-sm-12">
                                        <input id="demo-input-search2" type="text" placeholder="Tìm kiếm" class="form-control col-sm-
				                        8" autocomplete="off" ng-change="actions.filter()" ng-model="filter.freetext">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="category-table" class="table table-bordered table-hover table-vcenter">
                                <thead>
                                <tr>
                                    <th class="text-center">
                                        <input type="checkbox" ng-model="checker.btnCheckAll" ng-click="actions.checkAll(data.categories)">
                                    </th>
                                    <th class="text-center">#</th>
                                    <th>Mã tài xế</th>
                                    <th>Tên tài xế</th>
                                    <th>Tuổi</th>
                                    <th>Email</th>
                                    <th>Số điện thoại</th>
                                    <th>Loại xe</th>
                                    <th>Đội xe</th>
                                    <th>{!! trans('backend.category.action') !!}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="(key, driver) in data.drivers">
                                    <td style="width: 50px" class="text-center">
                                        <input type="checkbox" ng-model="checker.checkedAll[group.id]">
                                    </td>
                                    <td style="width: 50px"  class="text-center"> @{{ (data.page.current_page - 1) * data.page.per_page + key + 1 }} </td>
                                    <td> @{{ driver.code }} </td>
                                    <td> @{{ driver.user.name }} </td>
                                    <td> @{{ driver.old }} </td>
                                    <td> @{{ driver.user.email }} </td>
                                    <td> @{{ driver.user.phone }} </td>
                                    <td> @{{ driver.category_car }} </td>
                                    <td> @{{ driver.group.name }} </td>
                                    <td style="width: 180px">
                                        <a href="{{ url('admin/drivers') }}/@{{ driver.id }}/edit" class="btn btn-info btn-icon btn-sm" >
                                            <i class="fa-lg ti-pencil-alt"></i> {!! trans("backend.actions.edit") !!}
                                        </a>
{{--                                        <button class="btn btn-danger btn-sm btn-icon" ng-click="actions.delete(driver.id)">--}}
{{--                                            <i class="fa-lg ti-trash"></i> {!! trans("backend.actions.delete") !!}--}}
{{--                                        </button>--}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="text-center">
                                <div paging
                                     page="data.page.current_page"
                                     show-first-last="true"
                                     page-size="data.page.per_page"
                                     total="data.page.total"
                                     paging-action="actions.changePage(page)">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section ('myJs')
    <script src="{{ url('angularJs/uses/factory/services/driverService.js') }}"></script>
    <script src="{{ url('angularJs/uses/ctrls/driverCtrl.js') }}"></script>

    @if (Session::has('actions') && Session::get('actions') == 'success')
        <script>
            $.toast({
                heading: '{!! trans("confirm.success") !!}',
                text: 'Thao tác thành công',
                showHideTransition: 'fade',
                position: 'top-right',
                icon: 'success'
            })
        </script>
    @endif
@endsection
@section ('myCss')
@endsection

