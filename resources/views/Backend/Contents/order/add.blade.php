@extends('Backend.Layouts.default')
@section('content')
    <div id="content-container">
        <div id="page-head">
            <div id="page-title">
                <h1 class="page-header text-overflow">Đơn hàng</h1>
            </div>
            <ol class="breadcrumb">
                <li><a href="#"><i class="demo-pli-home"></i></a></li>
                <li><a href="#">{{ isset($order) ? 'Cập nhật' : 'Thêm mới' }}</a></li>
            </ol>
        </div>
        <div id="page-content">
            <div class="panel-body">
                <div class="panel">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-heading">
                                <h3 class="panel-title text-main text-bold mar-no">
                                    <i class="ti-pencil"></i> {{ isset($order) ? 'Cập nhật' : 'Thêm mới' }}
                                </h3>
                            </div>
                        </div>
                        <div class="panel-body col-sm-offset-2">
                            <!-- data-parsley-validate -->
                            @if (isset($order))
                                <form action="{{ route('orders.update', @$order->id) }}" method="POST" enctype="multipart/form-data" >
                                    @method ('PUT')
                            @else
                                <form action="{{ route('orders.store') }}" method="POST" enctype="multipart/form-data" >
                                    @method ('POST')
                            @endif
                                @csrf
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Mã đơn hàng
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" @if(@$order) readonly @endif  name="code" class="form-control"
                                                       value="{{ @$order->code ? $order->code : @old('code') }}">
                                                @if ($errors->has('code'))
                                                    <p class="text-left text-danger">{{ $errors->first('code') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Tên người nhận
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" name="info_name" class="form-control"
                                                       value="{{ @$order->info_name ? $order->info_name : @old('info_name') }}">
                                                @if ($errors->has('info_name'))
                                                    <p class="text-left text-danger">{{ $errors->first('info_name') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Số điện thoại người nhận
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="text"  name="info_phone" class="form-control"
                                                       value="{{ @$order->info_phone ? $order->info_phone : @old('info_phone') }}">
                                                @if ($errors->has('info_phone'))
                                                    <p class="text-left text-danger">{{ $errors->first('info_phone') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Khối lượng
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" name="weight" class="form-control"
                                                       value="{{ @$order->weight ? $order->weight : @old('weight') }}">
                                                @if ($errors->has('weight'))
                                                    <p class="text-left text-danger">{{ $errors->first('weight') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Điểm lấy hàng
                                                </label>
                                                <input type="text" name="address_form" class="form-control"
                                                       value="{{ @$order->address_form ? $order->address_form : @old('address_form') }}">
                                                @if ($errors->has('address_form'))
                                                    <p class="text-left text-danger">{{ $errors->first('address_form') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Điểm giao hàng
                                                </label>
                                                <input type="text" name="address_to" class="form-control"
                                                       value="{{ @$order->address_to ? $order->address_to : @old('address_to') }}">
                                                @if ($errors->has('address_to'))
                                                    <p class="text-left text-danger">{{ $errors->first('address_to') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Giá vận chuyển
                                                </label>
                                                <input type="text" @if(@$order) readonly @endif name="price" class="form-control"
                                                       value="{{ @$order->price ? $order->price : @old('price') }}">
                                                @if ($errors->has('price'))
                                                    <p class="text-left text-danger">{{ $errors->first('price') }}</p>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block btn-form-submit"><i class="ti-save"></i></button>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('myJs')
    <script>
        $('.selected-2').select2();
    </script>
@endsection