@extends('Backend.Layouts.default')
@section('content')
    <div id="content-container">
        <div id="page-head">
            <div id="page-title">
                <h1 class="page-header text-overflow"> Đội xe</h1>
            </div>
            <ol class="breadcrumb">
                <li><a href="#"><i class="demo-pli-home"></i></a></li>
                <li><a href="#">{{ isset($group) ? 'Cập nhật' : 'Thêm mới' }}</a></li>
            </ol>
        </div>
        <div id="page-content">
            <div class="panel-body">
                <div class="panel">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-heading">
                                <h3 class="panel-title text-main text-bold mar-no">
                                    <i class="ti-pencil"></i> {{ isset($group) ? 'Cập nhật' : 'Thêm mới' }}
                                </h3>
                            </div>
                        </div>
                        <div class="panel-body col-sm-offset-2">
                            <!-- data-parsley-validate -->
                            @if (isset($group))
                                <form action="{{ route('groups.update', @$group->id) }}" method="POST" enctype="multipart/form-data" >
                                    @method ('PUT')
                            @else
                                <form action="{{ route('groups.store') }}" method="POST" enctype="multipart/form-data" >
                                    @method ('POST')
                            @endif
                                @csrf
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Tên đội
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" name="name" class="form-control"
                                                       value="{{ @$group->name ? $group->name : @old('name') }}">
                                                @if ($errors->has('name'))
                                                    <p class="text-left text-danger">{{ $errors->first('name') }}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="control-label text-bold"> Mã đội
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="text" name="code" class="form-control"
                                                       value="{{ @$group->code ? $group->code : @old('code') }}">
                                                @if ($errors->has('code'))
                                                    <p class="text-left text-danger">{{ $errors->first('code') }}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block btn-form-submit"><i class="ti-save"></i></button>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

