ngApp.factory('$driverService', function ($http){

	var service = {
		action: {},
		data: {}
	};

	service.data.filter = function () {

	};

	service.action.get = function () {
		let url = SiteUrl + "/rest/admin/drivers";
        return $http.get(url);
	};

	service.action.delete = function ($id) {
        let url = SiteUrl + "/rest/admin/drivers/" + $id;
        return $http.delete(url);
	};

	return service;
})