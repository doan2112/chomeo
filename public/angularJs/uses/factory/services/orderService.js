ngApp.factory('$orderService', function ($http){

	var service = {
		action: {},
		data: {}
	};

	service.data.filter = function () {

	};

	service.action.get = function () {
		let url = SiteUrl + "/rest/admin/orders";
        return $http.get(url);
	};

	service.action.delete = function ($id) {
        let url = SiteUrl + "/rest/admin/orders/" + $id;
        return $http.delete(url);
	};

	return service;
})