ngApp.controller('groupCtrl',function($scope, $myNotify, $myBootbox, $myLoader, $groupService) {

	$scope.data = {
        groups: {},
		page: {}
	}
	$scope.filter = {
		freetext: ""
	}

	$scope.actions = {
		get: function () {
			let params = $groupService.data.filter($scope.filter.freetext, $scope.data.page.current_page);
			$groupService.action.get(params).then(function (resp) {
				if (resp) {
					$scope.data.groups = resp.data.data;
					$scope.data.page  = resp.data;
					if ($scope.data.page.current_page > resp.data.last_page) {
						$scope.data.page.current_page = resp.data.last_page;
						$scope.actions.get();
					}
				}
			}, function (error) {
			})
		},

		changePage: function (page) {
			$scope.data.page.current_page = page;
			$scope.actions.getAboutTeam();
		},

		delete: function ($id) {
			if ($id) {
				$myBootbox.confirm('Bạn có muốn xóa？', function (resp) {
					if (resp) {
					$groupService.action.delete($id).then(function (resp) {
						if (resp) {
							$myNotify.success('Thành công!');
							$scope.actions.get();
						}
						}, function (error) {
							$myNotify.error('Thất bại!');
						})
					}
				})
			}
		},

		filter: function () {
			$scope.actions.get();
		}

	}

	$scope.actions.get();
});