ngApp.controller('orderCtrl',function($scope, $myNotify, $myBootbox, $myLoader, $orderService) {

	$scope.data = {
        orders: {},
		page: {}
	}
	$scope.filter = {
		freetext: ""
	}

	$scope.actions = {
		get: function () {
			let params = $orderService.data.filter($scope.filter.freetext, $scope.data.page.current_page);
			$orderService.action.get(params).then(function (resp) {
				if (resp) {
					$scope.data.orders = resp.data.data;
					$scope.data.page  = resp.data;
					if ($scope.data.page.current_page > resp.data.last_page) {
						$scope.data.page.current_page = resp.data.last_page;
						$scope.actions.get();
					}
				}
			}, function (error) {
			})
		},

		changePage: function (page) {
			$scope.data.page.current_page = page;
			$scope.actions.getAboutTeam();
		},

		delete: function ($id) {
			if ($id) {
				$myBootbox.confirm('Bạn có muốn xóa？', function (resp) {
					if (resp) {
					$orderService.action.delete($id).then(function (resp) {
						if (resp) {
							$myNotify.success('Thành công!');
							$scope.actions.get();
						}
						}, function (error) {
							$myNotify.error('Thất bại!');
						})
					}
				})
			}
		},

		filter: function () {
			$scope.actions.get();
		}

	}

	$scope.actions.get();
});