<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('category')->nullable();
            $table->string('weight')->nullable();
            $table->string('info_name', 500)->nullable();
            $table->string('info_phone', 500)->nullable();
            $table->string('address_form')->nullable();
            $table->string('address_to')->nullable();
            $table->string('price')->nullable();
            $table->string('created_by')->nullable();
            $table->string('driver_id')->nullable();
            $table->boolean('status_payment')->nullable();
            $table->boolean('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
