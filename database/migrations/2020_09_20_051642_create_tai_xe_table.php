<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaiXeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->string('address')->nullable();
            $table->string('old')->nullable();
            $table->string('images')->nullable();
            $table->boolean('category_car')->nullable();
            $table->string('card_id')->nullable();
            $table->string('licence_id')->nullable();
            $table->string('license_plates')->nullable();
            $table->boolean('group_id')->nullable();
            $table->boolean('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
