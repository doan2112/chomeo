<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static paginate(int $int)
 * @method findOrFail(int $id)
 * @method find(int $id)
 */
class Order extends Model
{
    protected $table = "orders";
}
