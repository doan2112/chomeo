<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static paginate(int $int)
 * @method findOrFail(int $id)
 */
class Driver extends Model
{
    protected $table = "drivers";

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function group() {
        return $this->hasOne('App\Models\Group', 'id', 'group_id');
    }
}
