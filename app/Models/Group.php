<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method findOrFail(int $id)
 * @method where(string $string, int $id)
 * @method find(int $id)
 * @method static get()
 */
class Group extends Model
{
    protected $table = "groups";
}
