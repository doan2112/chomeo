<?php

namespace App\Http\Controllers\Backend;

use App\Models\Group;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class GroupController extends Controller
{
    private $groupModel;

    public function __construct() {
        $this->groupModel = new Group();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request) {

        $groups = $this->groupModel::paginate(10);
        return response()->json($groups);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index() {
        return view('Backend.Contents.group.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create() {

        $tpl = [];

        return view('Backend.Contents.group.add', $tpl);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->flash();
        $this->_validateInsert($request);
        DB::beginTransaction();

        try {
            $this->groupModel->name = $request->name;
            $this->groupModel->code = $request->code;
            $this->groupModel->save();

            DB::commit();
            return redirect()->route('groups.index')->with('actions', 'success');
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id) {

        $group = $this->groupModel->findOrFail($id);

        $tpl = [];

        $tpl['group'] = $group;

        return view('Backend.Contents.group.add', $tpl);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id) {
        $request->flash();
        $this->_validateInsert($request);
        DB::beginTransaction();
        $groupModel = $this->groupModel::findOrFail($id);

        try {
            $groupModel->name = $request->name;
            $groupModel->code = $request->code;
            $groupModel->save();

            DB::commit();
            return redirect()->route('groups.index')->with('actions', 'success');
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {
        DB::beginTransaction();
        try {
            $this->groupModel->find($id)->delete();
            DB::commit();
            return response()->json(['status' => false], 200);
        } catch (\Exception $e) {
            DB::rollback();
        }
    }


    public function _validateInsert($request) {
        $rules = array(
            'name' => 'between:1,255',
            'code' => 'between:1,255',
        );
        $messages = array();
        $attribute = array(
            'name' => 'Tên đội không được để trống',
            'code' => 'Mã dội không được để trống',
        );

        $this->validate($request, $rules, $messages, $attribute);
    }
}
