<?php

namespace App\Http\Controllers\Backend;

use App\Libs\Configs\StatusConfig;
use App\Models\Driver;
use App\Models\Group;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class DriverController extends Controller
{
    private $driverModel;
    private $groupModel;
    private $userModel;

    public function __construct() {
        $this->driverModel = new Driver();
        $this->userModel = new User();
        $this->groupModel = new Group();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request) {

        $drivers = $this->driverModel::with('user', 'group')->paginate(10);
        return response()->json($drivers);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index() {
        return view('Backend.Contents.driver.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create() {

        $tpl = [];

        $tpl['groups'] = $this->groupModel::get();

        $tpl['categories_car'] = [
            '1' =>'Xe máy',
            '2' => "Ô tô"
        ];


        return view('Backend.Contents.driver.add', $tpl);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->flash();
        $this->_validateInsert($request);
        DB::beginTransaction();


        try {
            $this->userModel->name     = $request->name;
            $this->userModel->email    = $request->email;
            $this->userModel->phone    = $request->phone;
            $this->userModel->password = Hash::make('123456');
            $this->userModel->status   = StatusConfig::CONST_AVAILABLE;
            $this->userModel->save();

            $this->driverModel->code = $request->code;
            $this->driverModel->address = $request->address;
            $this->driverModel->old = $request->old;
            $this->driverModel->images = $request->images;
            $this->driverModel->category_car = $request->category_car;
            $this->driverModel->card_id = $request->card_id;
            $this->driverModel->licence_id = $request->licence_id;
            $this->driverModel->license_plates = $request->license_plates;
            $this->driverModel->group_id = $request->group_id;

            $this->driverModel->user_id =  $this->userModel->id;
            $this->driverModel->save();

            DB::commit();
            return redirect()->route('drivers.index')->with('actions', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id) {

        $tpl = [];
        $tpl['driver'] = $this->driverModel->with('user')->findOrFail($id);
        $tpl['groups'] = $this->groupModel::get();
        $tpl['categories_car'] = [
            '1' =>'Xe máy',
            '2' => "Ô tô"
        ];

        return view('Backend.Contents.driver.add', $tpl);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id) {
        $request->flash();
        DB::beginTransaction();
        $driverModel = $this->driverModel->findOrFail($id);
        $userModel = $this->userModel->findOrFail($driverModel->user_id);

        try {
            $userModel->name     = $request->name;
            $userModel->phone    = $request->phone;
            $userModel->save();

            $driverModel->address = $request->address;
            $driverModel->old = $request->old;
            $driverModel->images = $request->images;
            $driverModel->category_car = $request->category_car;
            $driverModel->card_id = $request->card_id;
            $driverModel->licence_id = $request->licence_id;
            $driverModel->license_plates = $request->license_plates;
            $driverModel->group_id = $request->group_id;
            $driverModel->save();
            DB::commit();
            return redirect()->route('drivers.index')->with('actions', 'success');
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {
        DB::beginTransaction();
        try {
            $this->driverModel->find($id)->delete();
            DB::commit();
            return response()->json(['status' => false], 200);
        } catch (\Exception $e) {
            DB::rollback();
        }
    }


    public function _validateInsert($request) {
        $rules = array(
            'name' => 'between:1,255',
            'code' => 'between:1,255',
            'email' => 'required|between:1,255|unique:users,email',
        );
        $messages = array();
        $attribute = array(
            'name' => 'Tên đội không được để trống',
            'code' => 'Mã dội không được để trống',
        );

        $this->validate($request, $rules, $messages, $attribute);
    }
}
