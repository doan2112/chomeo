<?php

namespace App\Http\Controllers\Backend;

use App\Libs\Configs\StatusConfig;
use App\Models\Category;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class OrderController extends Controller
{
    private $categoryModel;
    private $orderModel;

    public function __construct() {
        $this->categoryModel = new Category();
        $this->orderModel = new Order();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request) {
        $orders = $this->orderModel::paginate(10);
        return response()->json($orders);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index() {
        return view('Backend.Contents.order.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create() {

        $tpl = [];

        $tpl['categories'] = $this->categoryModel::get();

        return view('Backend.Contents.order.add', $tpl);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {
        $request->flash();
        $this->_validateInsert($request);
        DB::beginTransaction();
        try {

            $this->orderModel->code = $request->code;
            $this->orderModel->weight = $request->weight;
            $this->orderModel->category = $request->category;
            $this->orderModel->address_form = $request->address_form;
            $this->orderModel->info_phone = $request->info_phone;
            $this->orderModel->info_name = $request->info_name;
            $this->orderModel->address_to = $request->address_to;
            $this->orderModel->price = $request->price;
            $this->orderModel->created_by = Auth::user()->id;
            $this->orderModel->status = 0;
            $this->orderModel->status_payment = 0;
            $this->orderModel->save();

            DB::commit();
            return redirect()->route('orders.index')->with('actions', 'success');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id) {

        $tpl = [];
        $tpl['order'] = $this->orderModel->findOrFail($id);

        return view('Backend.Contents.order.add', $tpl);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id) {
        $request->flash();
        DB::beginTransaction();
        $orderModel = $this->orderModel->findOrFail($id);

        try {
            $orderModel->weight = $request->weight;
            $orderModel->category = $request->category;
            $orderModel->address_form = $request->address_form;
            $orderModel->info_phone = $request->info_phone;
            $orderModel->info_name = $request->info_name;
            $orderModel->address_to = $request->address_to;
            $orderModel->save();
            DB::commit();
            return redirect()->route('orders.index')->with('actions', 'success');
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {
        DB::beginTransaction();
        try {
            $this->orderModel->find($id)->delete();
            DB::commit();
            return response()->json(['status' => false], 200);
        } catch (\Exception $e) {
            DB::rollback();
        }
    }


    public function _validateInsert($request) {
        $rules = array(
            'info_phone' => 'between:1,255',
            'code' => 'between:1,255',
            'info_name' => 'between:1,255',
        );
        $messages = array();
        $attribute = array(
            'info_phone' => 'Số điện thoại không được để trống',
            'code' => 'Mã đơn hàng không được để trống',
            'info_name' => 'Tên người nhận không được để trống',
        );

        $this->validate($request, $rules, $messages, $attribute);
    }
}
